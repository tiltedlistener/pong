package game;


import input.Direction;
import input.EnemyInput;
import input.InputHandler;
import input.InputInfo;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.ListIterator;

import javax.swing.JFrame;

import sound.Sound;
import sound.SoundPlayer;
import entities.*;

public class Game extends JFrame implements Runnable {

	// Settings
	private static final long serialVersionUID = 1L;
	private static String title = "The Pong";
	private int screenWidth = 1000;
	private int screenHeight = 500;
	
	// Loop
	private Thread gameLoop;
	private double previous = System.currentTimeMillis();
	private double lag = 0;
	private double MS_PER_UPDATE = 1000/24;	
	
	// Controls
	private InputHandler input = new InputHandler();
	private EnemyInput enemyInput = new EnemyInput();
	
	// Graphics
	protected Graphics2D g2d;
	public Graphics2D graphics() { return g2d; }
	protected BufferedImage backBuffer;		
	
	// Sprites
	private LinkedList<Sprite> sprites = new LinkedList<Sprite>();
	private Ball ball;
	private Paddle paddle;
	private Enemy enemy;
	private Wall top;
	private Wall bottom;
	
	// Scoreboard
	private Scoreboard board;
	
	// Sounds
	private SoundPlayer hit;
	private SoundPlayer music;
	
	// State
	private GameState currentState = GameState.START;
	private Image startScreen;
	private Image endScreen;

	public static void main(String[] args) {
		new Game();
	}
	
	public Game() {
		super(title);
		
		// Screen
		setSize(screenWidth, screenHeight);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);		
		
		// Graphics
		backBuffer = new BufferedImage(screenWidth, screenHeight, BufferedImage.TYPE_INT_RGB);
		g2d = backBuffer.createGraphics();		
		
		// Set up 
		ball = new Ball(graphics(), this);
		paddle = new Paddle(graphics(), this);
		enemy = new Enemy(graphics(), this);
		
		ball.setPos(new Vector2D(500, 250));
		paddle.setPos(new Vector2D(100, 150));
		enemy.setPos(new Vector2D(screenWidth - 100, 0));
		
		top = new Wall(new Vector2D(0,-74), new Vector2D(screenWidth, 100));
		bottom = new Wall(new Vector2D(0, screenHeight-5), new Vector2D(screenWidth, 100));
		
		ball.setVel(new Vector2D(-15, 5));
		
		board = new Scoreboard(graphics(), this);
		
		sprites.add(board);
		sprites.add(ball);
		sprites.add(paddle);
		sprites.add(enemy);
		
		// Intro 
		startScreen = ImageLoader.loadImage("images/start-screen.png");
		
		// Interface
		hit = new SoundPlayer(new Sound("sounds/hit.wav"));
		// music = new SoundPlayer(new Sound("sounds/music.wav"));
		// music.loop();
		
		// Input
		addKeyListener(input);
		
		// Threads
		gameLoop = new Thread(this);
		gameLoop.start();
		
		// Begin
		setVisible(true);
	}

	/**
	 * Game Loop
	 */	
	@Override
	public void run() {
		Thread t = Thread.currentThread();
		while(t == gameLoop) {
			double current = System.currentTimeMillis();
			double elapsed = current - previous;
			previous = current;
			lag += elapsed;
			
			while (lag >= MS_PER_UPDATE) {
				update();
				lag -= MS_PER_UPDATE;
			}	
			repaint();
		}				
	}

	public void stop() {
		gameLoop = null;
	}	

	// JFrame repaint method
	public void paint(Graphics g) {
		g2d.clearRect(0, 0, screenWidth, screenHeight);	
		render();
		g.drawImage(backBuffer, 0, 0, this);
	}	
	
	public void menuScreenUpdate() {
		InputInfo currentCode = input.handleInput();	
		if (currentState == GameState.START) {
			if (currentCode == InputInfo.ENTER_RELEASED) {
				currentState = GameState.PLAY;
			}
		} else if (currentState == GameState.END) {
			if (currentCode == InputInfo.ENTER_RELEASED) {
				currentState = GameState.START;
			}			
		}
	}
	
	public void render() {
		if (currentState == GameState.PLAY) {
			gameDraw();
		} else if (currentState == GameState.START) {
			startScreenDraw();
		} else if (currentState == GameState.END) {
			endScreenDraw();
		}
	}	
	
	public void startScreenDraw() {
		g2d.drawImage(startScreen, 0, 0, startScreen.getWidth(this), startScreen.getHeight(this), this);
	}
	
	public void endScreenDraw() {
		g2d.drawImage(endScreen, 0, 0, startScreen.getWidth(this), startScreen.getHeight(this), this);		
	}
	
	public void gameDraw() {	
		g2d.setColor(Color.WHITE);
        g2d.setStroke(new BasicStroke(10));
		g2d.drawLine(0, 26, screenWidth, 26);
		g2d.drawLine(0, screenHeight - 5, screenWidth, screenHeight - 5);		
		
		ListIterator<Sprite> listIterator = sprites.listIterator();
        while (listIterator.hasNext()) {
			Sprite current = listIterator.next();
			if (current.isAlive()) {
				current.draw();
			}
        }			
	}
	
	// Update
	public void update() {
		if (currentState == GameState.PLAY) {
			gameUpdate();
		} else if (currentState == GameState.START || currentState == GameState.END) {
			menuScreenUpdate();
		}
	}
	
	public void gameUpdate() {
		spriteCleanUp();		
		InputInfo currentInput = input.handleInput();

		ListIterator<Sprite> listIterator = sprites.listIterator();
        while (listIterator.hasNext()) {
        	Sprite current = listIterator.next();
			if (current instanceof Paddle) {
				Paddle paddle = (Paddle)current;
				paddle.update(currentInput);
			} else if (current instanceof Enemy){
				Direction dir = enemyInput.setDirection(ball, enemy);
				if (dir != null) {
					if (dir.isDown()) {
						enemy.setVel(new Vector2D(0, 20));
					} else if (dir.isUp()) {
						enemy.setVel(new Vector2D(0,-20));
					} 					
				} else {
					enemy.setVel(new Vector2D(0, 0));
				}
				enemy.update();
			} else {	
				current.update();
			}
        }		
		
		// Concerns hitting other in game objects
		checkSpriteCollisions();		
		
		// Check for Ball Leaving field
		checkForScore();
	}	
	
	public void checkSpriteCollisions() {
		// There's probably a math equation for this
		if (rectangleCollisionCheck(paddle, ball)) {
			Vector2D vel = ball.getVel();
			Vector2D oppVel = paddle.getVel().clone();
			oppVel.invert();
			vel.add(oppVel);
			vel.invert();
			ball.setVel(vel);
			hit.start();
		} else if (rectangleCollisionCheck(enemy, ball)) {
			Vector2D vel = ball.getVel();
			Vector2D oppVel = enemy.getVel().clone();
			oppVel.invert();
			vel.add(oppVel);
			vel.invert();
			ball.setVel(vel);
			hit.start();
		} else if (rectangleCollisionCheck(top, ball) || rectangleCollisionCheck(bottom, ball)) {
			Vector2D vel = ball.getVel();
			ball.setVel(new Vector2D(vel.X(), -vel.Y()));	
			hit.start();
		}
	}
	
	private boolean rectangleCollisionCheck(Collides outer, Collides inner) {
		Vector2D aOrigin = outer.getPos();
		Vector2D aSize = outer.getSize();
		double aLeft = aOrigin.X();
		double aRight = aOrigin.X() + aSize.X();
		double aBottom = aOrigin.Y();
		double aTop = aOrigin.Y() + aSize.Y();
		
		Vector2D bOrigin = inner.getPos();
		Vector2D bSize = inner.getSize();
		double bLeft = bOrigin.X();
		double bRight = bOrigin.X() + bSize.X();
		double bBottom = bOrigin.Y();
		double bTop = bOrigin.Y() + bSize.Y();		

		return overlappingRectangles(aLeft, aRight, bLeft, bRight) && overlappingRectangles(aBottom, aTop, bBottom, bTop);
	}
	
	private boolean overlappingRectangles(double minA, double maxA, double minB, double maxB) {
		return minB <= maxA && minA <= maxB;
	}
	
	private void checkForScore() {
		if (ball.getPos().X() <= 0) {
			resetBall("player");			
			board.addEnemeyScore();
		} else if (ball.getPos().X() >= screenWidth) {
			resetBall("enemy");
			board.addPaddleScore();
		}
	}
	
	private void resetBall(String type) {
		ball.setPos(new Vector2D(500, 250));
		int vertical = (int)(Math.random() * 5);
		if (Math.random() > 0.5) {
			vertical *= -1;
		}
		
		if (type == "player") {
			ball.setVel(new Vector2D(-15, vertical));
		} else {
			ball.setVel(new Vector2D(15, vertical));
		}
	}
	
	public void spriteCleanUp() {
		for (int i = 0; i < sprites.size(); i++) {
			if (!sprites.get(i).isAlive()) {
				sprites.remove(i);
			}
		}
	}
	
	public void endGame() {
		gameReset();
		currentState = GameState.END;
	}
	
	public void gameReset() {

	}
}
