package game;

public class Vector2D {

	private double x;
	private double y;
	
	public Vector2D() {
		x = 0;
		y = 0;
	}
	
	public Vector2D(double _x, double _y) {
		x = _x;
		y = _y;
	}
	
	public void setValues(double _x, double _y) {
		x = _x;
		y = _y;
	}
	
	public double X() {
		return x;
	}
	
	public double Y() {
		return y;
	}
	
	public void add(Vector2D vec) {
		x += vec.X();
		y += vec.Y();
	}
	
	public void sub(Vector2D vec) {
		x -= vec.X();
		y -= vec.Y();
	}
	
	public Vector2D clone() {
		return new Vector2D(x, y);
	}
	
	public void invert() {
		x = -x;
		y = -y;
	}
	
	public boolean overlap(Vector2D _vec) {
		if (x == _vec.x && y == _vec.y) return true;
		return false;
	}
	
}
