package sound;

public class SoundPlayer implements Runnable {

	private Sound tempSound;
	private Thread t;
	private boolean loop = false;
	
	public SoundPlayer(Sound newSound) {
		tempSound = newSound;
	}
	
	public void loop() {
		loop = true;
	}
	
	public void start() {
		if (t == null) {
			t = new Thread(this, "soundThread" + System.currentTimeMillis());
			t.start();
		}
	}
	
	@Override
	public void run() {		

		while(t != null) {
			tempSound.play();
			if (loop) {
				if (tempSound.isStopped()) {
					tempSound.play();
				}
			} else {
				if (tempSound.isStopped()) {
					t = null;
				}				
			}
		}
	}

}
