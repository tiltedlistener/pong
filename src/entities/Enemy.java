package entities;

import game.Vector2D;

import java.awt.Color;
import java.awt.Graphics2D;

import javax.swing.JFrame;

public class Enemy extends Sprite {
	
	public Enemy(Graphics2D _g, JFrame _frame) {
		super(_g, _frame);
		size = new Vector2D(10, 100);
		density = 1;
	}
	
	@Override
	public void draw() {
		g2d.setColor(Color.WHITE);
		g2d.fillRect((int)pos.X(), (int)pos.Y(),(int)size.X(), (int)size.Y());		
	}
	
	public void update() {
		pos.add(vel);
		
		if (pos.Y() + size.Y() >= 500) {
			setVel(new Vector2D(0, 0));
			setPos(new Vector2D(pos.X(), 500 - size.Y()));
		} else if (pos.Y() <= 26) {
			setVel(new Vector2D(0, 0));
			setPos(new Vector2D(pos.X(), 26));
		}		
	}
}
