package entities;

import java.awt.BasicStroke;
import java.awt.Font;
import java.awt.Graphics2D;

import javax.swing.JFrame;

import game.Vector2D;

public class Scoreboard extends Sprite {

	private Vector2D pos = new Vector2D(500, 90);
	
	// Points
	private int paddleScore = 0;
	private int enemyScore = 0;
	private Font myFont = new Font("Courier New", 1, 50);
	
	
	public Scoreboard(Graphics2D _g, JFrame _frame) {
		super(_g, _frame);
	}
	
	@Override
	public void draw() {
		g2d.setStroke(new BasicStroke(1));
		g2d.drawLine(500, 0, 500, 500);
		g2d.setFont(myFont); 
		g2d.drawString(String.valueOf(paddleScore), (int)pos.X() -66, (int)pos.Y());
		g2d.drawString(String.valueOf(enemyScore), (int)pos.X() + 33, (int)pos.Y());
	}
	
	public void addPaddleScore() {
		paddleScore++;
	}
	
	public void addEnemeyScore() {
		enemyScore++;
	}
	
}
