package entities;

import game.Vector2D;

import java.awt.Color;
import java.awt.Graphics2D;

import javax.swing.JFrame;

public class Ball extends Sprite {

	public Ball(Graphics2D _g, JFrame _frame) {
		super(_g, _frame);
		size = new Vector2D(10, 10);
	}
	
	@Override
	public void draw() {
		g2d.setColor(Color.WHITE);
		g2d.fillRect((int)pos.X(), (int)pos.Y(),(int)size.X(), (int)size.Y());	
	}
	
	@Override
	public void update() {
		pos.add(vel);
	}

}
