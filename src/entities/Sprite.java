package entities;

import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.JFrame;

import game.Vector2D;

public class Sprite implements Collides {

	// State
	protected Vector2D pos;
	protected Vector2D size = new Vector2D(0, 0);
	protected Vector2D vel = new Vector2D(0, 0);
	protected boolean alive = true;
	public double density = 0;
	
	// Graphical contexts
	protected JFrame frame;
	protected Graphics2D g2d;
	protected Image image;
	
	public Sprite(Graphics2D _g, JFrame _frame) {
		frame = _frame;
		g2d = _g;
	}	
	
	public void setImage(Image _img) {
		image = _img;
	}
	
	public void setPos(Vector2D _vec) {
		pos = _vec;
	}
	
	public void adjVel(Vector2D _vec) {
		vel.add(_vec);
	}
	
	public void setVel(Vector2D _vec) {
		vel = _vec;
	}
	
	public Vector2D getVel() {
		return vel;
	}
	
	public Vector2D getPos() {
		return pos;
	}
	
	public Vector2D getSize() {
		return size;
	}
	
	public boolean isAlive() {
		return alive;
	}
	
	public void kill() {
		alive = false;
	}
	
	public void update() {}
	
	public void draw() {
		g2d.drawImage(image, (int)pos.X(), (int)pos.Y(), image.getWidth(frame), image.getHeight(frame), frame);
	}
	
}
