package entities;

import game.Vector2D;

public interface Collides {
	public Vector2D getPos();
	public Vector2D getSize();
}
