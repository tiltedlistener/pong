package entities;

import game.Vector2D;

public class Wall implements Collides {

	private Vector2D pos;
	private Vector2D size;
	
	public Wall(Vector2D _pos, Vector2D _size) {
		pos = _pos;
		size = _size;
	}

	@Override
	public Vector2D getPos() {
		return pos;
	}

	@Override
	public Vector2D getSize() {
		return size;
	}
	
}
