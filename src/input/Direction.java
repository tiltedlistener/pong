package input;

public class Direction {

	private boolean down = false;
	private boolean up = false;
	
	public Direction(boolean _down, boolean _up) {
		down = _down;
		up = _up;
	}
	
	public boolean isDown() {
		return down;
	}
	
	public boolean isUp() {
		return up;
	}
}
