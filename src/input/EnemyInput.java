package input;

import entities.Ball;
import entities.Enemy;
import game.Vector2D;

public class EnemyInput {
	
	// Based on http://www.gamedev.net/topic/439576-pong-ai/
	// stationary bounce / English down / English up
	
	// http://javacooperation.gmxhome.de/PongKIEng.html
	
	public Direction setDirection(Ball _ball, Enemy _enemy) {
		Vector2D vel = _ball.getVel();
		Vector2D pos = _ball.getPos();
		Vector2D enemyPos = _enemy.getPos();
		
		if (vel.X() > 0) {
			Vector2D nextPos = predictPosition(pos, vel);
			Direction dir = positionIsInRange(nextPos, enemyPos);
			return dir;
		} else {
			return null;
		}
	}
	
	public Vector2D predictPosition(Vector2D _pos, Vector2D _vel) {
		Vector2D pos = _pos.clone();
		pos.add(_vel);
		return pos;
	}
	
	public Direction positionIsInRange(Vector2D _pos, Vector2D _emPos) {
		if (_pos.Y() < _emPos.Y()) {
			return new Direction(false, true);
		} else if (_pos.Y() > _emPos.Y() + 50) {
			return new Direction(true, false);
		} 
		return null;
	}
}
